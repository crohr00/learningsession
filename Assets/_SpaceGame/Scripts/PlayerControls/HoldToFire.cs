﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoldToFire : MonoBehaviour
{
    [SerializeField] private float _fireRate = .25f;
    [SerializeField] private GameObject _prefab;
    [SerializeField] private AudioSource _audio;
    private float _timeToFire = 0;

    private void Update()
    {
        if ((Input.GetMouseButton(0) || Input.GetKey(KeyCode.Space)) && _timeToFire <= Time.time)
        {
            Fire();
            _timeToFire = _fireRate + Time.time;
        }
    }

    private void Fire()
    {
        _audio.Play();
        GameObject.Instantiate(_prefab, transform.position, Quaternion.identity);
    }
}
