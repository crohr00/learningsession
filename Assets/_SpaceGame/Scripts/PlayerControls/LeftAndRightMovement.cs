﻿using UnityEngine;
using UnityEngine.Networking;

public class LeftAndRightMovement : MonoBehaviour
{
    [SerializeField] private Rect _bounds;

	private void Update ()
	{
	    if ((Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow)) && transform.position.x >= _bounds.min.x)
	    {
	        transform.GetChild(0).localEulerAngles = new Vector3(0, 11, 0);
	        transform.Translate(new Vector3(-0.25f, 0, 0));
	    }
	    else if ((Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow)) && transform.position.x <= _bounds.xMax)
	    {
	        transform.GetChild(0).localEulerAngles = new Vector3(0, -11, 0);
	        transform.Translate(new Vector3(.25f, 0, 0));
	    }
	    else
	    {
	        transform.GetChild(0).localEulerAngles = new Vector3(0, 0, 0);
	    }
	}
}
