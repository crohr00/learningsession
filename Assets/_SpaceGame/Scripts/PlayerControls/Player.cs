﻿using UnityEngine;

public class Player : ExplodeOnDeath
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy")
        {
            OnDeath();
            GameManager.Instance.PlayerDied();
            Destroy(gameObject);
        }
    }
}
