﻿using System;
using UnityEngine;
using UnityEngine.Video;
using Random = UnityEngine.Random;

public class Asteriod : ExplodeOnDeath
{
    [SerializeField] private float _speed = .15f;
    [SerializeField] private float _maxSize = 2;
    [SerializeField] private int _scoreMultiplier = 10;
    private GameObject _player;
    private Vector3 _finalTarget;
    private int _pointValue = 0;

    private void Awake()
    {
        var sizeMultiplier = 1 + Random.value;
        _pointValue = (int)Math.Ceiling(sizeMultiplier * _scoreMultiplier);
        transform.localScale *= sizeMultiplier * _maxSize;
        _player = GameObject.Find("Player");
        _finalTarget = new Vector3(Random.Range(-8.5f, 8.5f), _player.transform.position.y - 20, 0);
    }

    private void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, _finalTarget, Time.deltaTime * _speed);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" || other.tag == "Projectile")
        {
            ScoreManager.IncreaeScore(_pointValue);
            OnDeath();
            Destroy(gameObject);
        }
    }
}
