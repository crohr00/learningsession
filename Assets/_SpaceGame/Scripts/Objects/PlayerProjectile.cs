﻿using UnityEngine;

public class PlayerProjectile : ExplodeOnDeath
{
    [SerializeField] private float _speed = .25f;

	private void Update()
	{
		transform.Translate(new Vector3(0, _speed, 0));
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy")
        {
            OnDeath();
            Destroy(gameObject);
        }
    }
}
