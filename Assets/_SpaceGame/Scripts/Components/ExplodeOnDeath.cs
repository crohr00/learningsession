﻿using UnityEngine;

public class ExplodeOnDeath : MonoBehaviour
{
    [SerializeField] private GameObject _explosion;

    // This is bad and should never be done
    protected void OnDeath()
    {
        var go = GameObject.Instantiate(_explosion, transform.position, Quaternion.identity);
        Destroy(go, 3);
    }
}
