﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAfterSeconds : MonoBehaviour
{
    [SerializeField] private float _seconds = 5;
    private float _destroyAtTime;

    private void Start()
    {
        _destroyAtTime = Time.time + _seconds;
    }

    private void Update()
    {
        if(_destroyAtTime < Time.time)
        {
            Destroy(gameObject);
        }
    }
}
