﻿using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    [SerializeField] private Text _scoreText;
    private static int _totalPoints;

    public static void IncreaeScore(int points)
    {
        _totalPoints += points;
    }

    public static void Reset()
    {
        _totalPoints = 0;
    }

    private void Update()
    {
        _scoreText.text = _totalPoints.ToString();
    }
}
