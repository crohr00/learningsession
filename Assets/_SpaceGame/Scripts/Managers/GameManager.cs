﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public bool GameOver = true;

    #region Singleton
    private static GameManager _instance;
    public static GameManager Instance
    {
        get { return _instance; }
    }

    private void Awake()
    {
        if (_instance != null)
        {
            Destroy(gameObject);
            return;
        }
        _instance = this;
    }
    #endregion

    public void PlayerDied()
    {
        GameOver = true;
        StartCoroutine(Wait());
    }

    private IEnumerator Wait()
    {
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene("GameOver");
    }

    private void Update()
    {
        if(GameOver && SceneManager.GetActiveScene().name == "GameStart" || SceneManager.GetActiveScene().name == "GameOver")
        {
            if (Input.anyKeyDown && !(Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1)))
            {
                StartGame();
            }
        }
    }

    private void StartGame()
    {
        ScoreManager.Reset();
        GameOver = false;
        SceneManager.LoadScene("SpaceGame");
    }
}
