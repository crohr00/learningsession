﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField] private List<GameObject> _obstacles;
    [SerializeField] private float _spawnInterval;

    private float _nextSpawnTime = 0;

    private void Update()
    {
        if (_nextSpawnTime < Time.time && !GameManager.Instance.GameOver)
        {
            var rnd = Random.Range(0, 100);
            if (rnd < 15)
            {
                Spawn();
            }
        }
    }

    private void Spawn()
    {
        var index = Random.Range(0, _obstacles.Count);
        var spawnOffset = Random.insideUnitCircle * 8.5f;
        spawnOffset *= (((int) Time.time) / 2 == 0) ? -1 : 1;
        var spawnPosition = transform.position;
        spawnPosition.x += spawnOffset.x;
        spawnPosition.y += spawnOffset.y;

        GameObject.Instantiate(_obstacles[index], spawnPosition, Quaternion.identity, transform);
        _nextSpawnTime = Time.time + _spawnInterval;
    }
}
